
import {loginUrl} from  './src/controllers/login';

addEventListener('fetch', event => {
    event.respondWith(handleRequest(event.request))
})

/**
 * Fetch and log a request
 * @param {Request} request
 */
async function handleRequest(request) {
    return new Response('Login URL: ' + loginUrl(), { status: 200 })
}
