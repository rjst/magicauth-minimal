var webpack = require('webpack');

module.exports = {
    entry: "./index.js",
    mode: "production", //development
    devtool: 'cheap-module-source-map',

    optimization: {
        minimize: true //false
    },
    performance: {
        hints: false
    },
    output: {
        path: __dirname + "/dist",
        publicPath: "dist",
        filename: "main.js"
    },
    plugins: [
        new webpack.IgnorePlugin(/tls/),
        new webpack.IgnorePlugin(/net/),
    ],
    node: {
        fs: 'empty',
        child_process: 'empty'
    }
};
