import express from 'express';
import logger from 'morgan';
import indexRouter from './routes/index';
import loginRouter from './routes/login';

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/', indexRouter);
app.use('/login', loginRouter);


module.exports = app;
