import express from 'express';
const router = express.Router();

import {loginUrl} from  './../controllers/login';

/* GET home page. */
router.get('/', function(req, res, next) {
    res.json({loginUrl:loginUrl()});
});

export default router;
